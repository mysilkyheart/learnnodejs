const Products = require('../models/products')

exports.list = (req, res, next) => {
    Products.fetchAll(products => {
        res.render('list-product', { products })
    });
}

exports.addProductView = (req, res) => {
    res.render('add-product')
}

exports.addProduct = (req, res) => {
    const product = new Products(req.body.title)
    product.save();
    res.redirect('/shop/product')
}