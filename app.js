const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const adminRoute = require('./routes/admin')
const shop = require('./routes/shop')


app.set('view engine', 'ejs')

app.use(bodyParser.urlencoded({extended: false}))
app.use('/shop', shop.router)
app.use('/admin', adminRoute)

app.use((req, res) => { 
    res.status(404).render('404', {pageTitle: 'Halaman Tidak Ditemukan'})
})

app.listen(3000)