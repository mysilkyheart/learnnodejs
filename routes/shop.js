const express = require('express')
const router = express.Router()
const productController = require('../controllers/productController')

router.get('/product', productController.list)
router.get('/add-product', productController.addProductView)
router.post('/add-product', productController.addProduct)

exports.router = router